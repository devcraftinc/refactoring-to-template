// Copyright 2018-2018 DevCraft, Inc.
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

package katas ;

import katas.heads.*;
import katas.feet.*;
import katas.arms.*;
import katas.equipment.*;
import katas.torsos.*;
import katas.legs.*;

  public class SkyKnightRobotBuilder implements RobotBuilder
  {
    private SkyKnightRobotBuilder()
    {
    }

    public Robot build()
    {
      Part c = new Part(null);
      JetpackTorso torso = new JetpackTorso(c);
      CockpitHead head = new CockpitHead(c);
      Joint neckJoint = new Joint(c, torso, head);
      LongRangeMissile missileLauncher = new LongRangeMissile(c);
      Joint rightShoulderMountJoint = new Joint(c, torso, missileLauncher);
      StockArm leftArm = new StockArm(c);
      Joint leftShoulderJoint = new Joint(c, torso, leftArm);
      StockArm rightArm = new StockArm(c);
      Joint rightShoulderJoint = new Joint(c, torso, rightArm);
      Shield leftHand = new Shield(c);
      Joint leftWristJoint = new Joint(c, leftArm, leftHand);
      Sword rightHand = new Sword(c);
      Joint rightWristJoint = new Joint(c, rightArm, rightHand);
      StockLeg leftLeg = new StockLeg(c);
      Joint leftHipJoint = new Joint(c, torso, leftLeg);
      StockLeg rightLeg = new StockLeg(c);
      Joint rightHipJoint = new Joint(c, torso, rightLeg);
      ThrusterFoot leftFoot = new ThrusterFoot(c);
      Joint leftAnkleJoint = new Joint(c, leftLeg, leftFoot);
      ThrusterFoot rightFoot = new ThrusterFoot(c);
      Joint rightAnkleJoint = new Joint(c, rightLeg, rightFoot);
      return new Robot(
        c, torso,
        neckJoint, head,
        null, null, rightShoulderMountJoint, missileLauncher,
        leftShoulderJoint, leftArm, rightShoulderJoint, rightArm,
        leftWristJoint, leftHand, rightWristJoint, rightHand,
        leftHipJoint, leftLeg, rightHipJoint, rightLeg,
        leftAnkleJoint, leftFoot, rightAnkleJoint, rightFoot);
    }

    public static RobotBuilder getInstance()
    {
      return new SkyKnightRobotBuilder();
    }
  }


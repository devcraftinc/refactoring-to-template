// Copyright 2018-2018 DevCraft, Inc.
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

package katas;

public class Robot {
    public Robot(Part containerPart, Part torso, Joint neckJoint, Part head, Joint leftShoulderMountJoint,
                 Part leftShoulderMount, Joint rightShoulderMountJoint, Part rightShoulderMount, Joint leftShoulderJoint,
                 Part leftArm, Joint rightShoulderJoint, Part rightArm, Joint leftWristJoint, Part leftHand, Joint rightWristJoint,
                 Part rightHand, Joint leftHipJoint, Part leftLeg, Joint rightHipJoint, Part rightLeg, Joint leftAnkleJoint,
                 Part leftFoot, Joint rightAnkleJoint, Part rightFoot) {
        this.containerPart = containerPart;
        this.torso = torso;
        this.neckJoint = neckJoint;
        this.head = head;
        this.leftShoulderMountJoint = leftShoulderMountJoint;
        this.leftShoulderMount = leftShoulderMount;
        this.rightShoulderMountJoint = rightShoulderMountJoint;
        this.rightShoulderMount = rightShoulderMount;
        this.leftShoulderJoint = leftShoulderJoint;
        this.leftArm = leftArm;
        this.rightShoulderJoint = rightShoulderJoint;
        this.rightArm = rightArm;
        this.leftWristJoint = leftWristJoint;
        this.leftHand = leftHand;
        this.rightWristJoint = rightWristJoint;
        this.rightHand = rightHand;
        this.leftHipJoint = leftHipJoint;
        this.leftLeg = leftLeg;
        this.rightHipJoint = rightHipJoint;
        this.rightLeg = rightLeg;
        this.leftAnkleJoint = leftAnkleJoint;
        this.leftFoot = leftFoot;
        this.rightAnkleJoint = rightAnkleJoint;
        this.rightFoot = rightFoot;
    }

    private Part containerPart;

    public Part getContainerPart() {
        return containerPart;
    }

    private Part torso;

    public Part getTorso() {
        return torso;
    }

    private Joint neckJoint;

    public Joint getNeckJoint() {
        return neckJoint;
    }

    private Part head;

    public Part getHead() {
        return head;
    }

    private Joint leftShoulderMountJoint;

    public Joint getLeftShoulderMountJoint() {
        return leftShoulderMountJoint;
    }

    private Part leftShoulderMount;

    public Part getLeftShoulderMount() {
        return leftShoulderMount;
    }

    private Joint rightShoulderMountJoint;

    public Joint getRightShoulderMountJoint() {
        return rightShoulderMountJoint;
    }

    private Part rightShoulderMount;

    public Part getRightShoulderMount() {
        return rightShoulderMount;
    }

    private Joint leftShoulderJoint;

    public Joint getLeftShoulderJoint() {
        return leftShoulderJoint;
    }

    private Part leftArm;

    public Part getLeftArm() {
        return leftArm;
    }

    private Joint rightShoulderJoint;

    public Joint getRightShoulderJoint() {
        return rightShoulderJoint;
    }

    private Part rightArm;

    public Part getRightArm() {
        return rightArm;
    }

    private Joint leftWristJoint;

    public Joint getLeftWristJoint() {
        return leftWristJoint;
    }

    private Part leftHand;

    public Part getLeftHand() {
        return leftHand;
    }

    private Joint rightWristJoint;

    public Joint getRightWristJoint() {
        return rightWristJoint;
    }

    private Part rightHand;

    public Part getRightHand() {
        return rightHand;
    }

    private Joint leftHipJoint;

    public Joint getLeftHipJoint() {
        return leftHipJoint;
    }

    private Part leftLeg;

    public Part getLeftLeg() {
        return leftLeg;
    }

    private Joint rightHipJoint;

    public Joint getRightHipJoint() {
        return rightHipJoint;
    }

    private Part rightLeg;

    public Part getRightLeg() {
        return rightLeg;
    }

    private Joint leftAnkleJoint;

    public Joint getLeftAnkleJoint() {
        return leftAnkleJoint;
    }

    private Part leftFoot;

    public Part getLeftFoot() {
        return leftFoot;
    }

    private Joint rightAnkleJoint;

    public Joint getRightAnkleJoint() {
        return rightAnkleJoint;
    }

    private Part rightFoot;

    public Part getRightFoot() {
        return rightFoot;
    }
}


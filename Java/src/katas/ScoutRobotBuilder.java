// Copyright 2018-2018 DevCraft, Inc.
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.





package katas ;

import katas.equipment.*;
import katas.torsos.*;
import katas.legs.*;

public class ScoutRobotBuilder implements RobotBuilder
  {
    private ScoutRobotBuilder()
    {
    }

    public Robot build()
    {
      Part c = new Part(null);
      LightCockpitTorso torso = new LightCockpitTorso(c);
      Laser laser = new Laser(c);
      Joint leftShoulderMountJoint = new Joint(c, torso, laser);
      SensorPod sensorPod = new SensorPod(c);
      Joint rightShoulderMountJoint = new Joint(c, torso, sensorPod);
      FastTread leftLeg = new FastTread(c);
      Joint leftHipJoint = new Joint(c, torso, leftLeg);
      FastTread rightLeg = new FastTread(c);
      Joint rightHipJoint = new Joint(c, torso, rightLeg);
      return new Robot(
        c, torso,
        null, null,
        leftShoulderMountJoint, laser, rightShoulderMountJoint, sensorPod,
        null, null, null, null,
        null, null, null, null,
        leftHipJoint, leftLeg, rightHipJoint, rightLeg,
        null, null, null, null);
    }

    public static RobotBuilder getInstance()
    {
      return new ScoutRobotBuilder();
    }
  }


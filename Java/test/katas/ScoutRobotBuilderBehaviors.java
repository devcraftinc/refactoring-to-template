// Copyright 2018-2018 DevCraft, Inc.
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.


package katas;


import katas.equipment.*;
import katas.legs.*;
import katas.torsos.*;
import org.junit.Test;

import static org.hamcrest.CoreMatchers.*;
import static org.junit.Assert.assertThat;

public class ScoutRobotBuilderBehaviors extends RobotBuilderBehaviorsBase {
    @Test
    public void laserLeftShoulderMount() {
        assertThat(Robot.getLeftShoulderMount(), is(instanceOf(Laser.class)));
    }

    @Test
    public void sensorPodRightShoulderMount() {
        assertThat(Robot.getRightShoulderMount(), is(instanceOf(SensorPod.class)));
    }

    @Test
    public void lightweight() {
        noPartsFor(Robot.getHead(), Robot.getLeftArm(), Robot.getRightArm(), Robot.getLeftHand(), Robot.getRightHand(), Robot.getLeftFoot(),
                Robot.getRightFoot());
    }

    @Test
    public void lightweightTorso() {
        assertThat(Robot.getTorso(), is(instanceOf(LightCockpitTorso.class)));
    }

    @Test
    public void fastTreadLegs() {
        assertThat(Robot.getLeftLeg(), is(instanceOf(FastTread.class)));
        assertThat(Robot.getRightLeg(), is(instanceOf(FastTread.class)));
    }

    @Override
    protected RobotBuilder getRobotBuilder() {
        return ScoutRobotBuilder.getInstance();
    }
}


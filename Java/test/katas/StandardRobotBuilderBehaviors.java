// Copyright 2018-2018 DevCraft, Inc.
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.


package katas;

import katas.arms.*;
import katas.feet.*;
import katas.hands.*;
import katas.heads.*;
import katas.legs.*;
import katas.torsos.*;
import org.junit.Test;

import static org.hamcrest.CoreMatchers.*;
import static org.junit.Assert.assertThat;


public class StandardRobotBuilderBehaviors extends RobotBuilderBehaviorsBase {
    @Test
    public void noShoulderMounts() {
        noPartsFor(Robot.getLeftShoulderMount(), Robot.getRightShoulderMount());
    }

    @Test
    public void cockpitHead() {
        assertThat(Robot.getHead(), is(instanceOf(CockpitHead.class)));
    }

    @Test
    public void standardTorso() {
        assertThat(Robot.getTorso(), is(instanceOf(StockTorso.class)));
    }

    @Test
    public void standardLegs() {
        assertThat(Robot.getLeftLeg(), is(instanceOf(StockLeg.class)));
        assertThat(Robot.getRightLeg(), is(instanceOf(StockLeg.class)));
    }

    @Test
    public void standardFeet() {
        assertThat(Robot.getLeftFoot(), is(instanceOf(StockFoot.class)));
        assertThat(Robot.getRightFoot(), is(instanceOf(StockFoot.class)));
    }

    @Test
    public void standardArms() {
        assertThat(Robot.getLeftArm(), is(instanceOf(StockArm.class)));
        assertThat(Robot.getRightArm(), is(instanceOf(StockArm.class)));
    }

    @Test
    public void clampHands() {
        assertThat(Robot.getLeftHand(), is(instanceOf(ClampHand.class)));
        assertThat(Robot.getRightHand(), is(instanceOf(ClampHand.class)));
    }

    @Override
    protected RobotBuilder getRobotBuilder() {
        return StandardRobotBuilder.getInstance();
    }
}


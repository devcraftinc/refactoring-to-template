// Copyright 2018-2018 DevCraft, Inc.
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.


package katas;


import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;

import static org.hamcrest.CoreMatchers.*;
import static org.hamcrest.MatcherAssert.assertThat;

public abstract class RobotBuilderBehaviorsBase {
    private ArrayList<Part> nonNullParts;
    protected Robot Robot;

    @Before
    public void setup() {
        Robot = getRobotBuilder().build();
        nonNullParts = new ArrayList<>();
        for (Part part : new Part[]
                {
                        Robot.getHead(), Robot.getTorso(), Robot.getLeftArm(), Robot.getRightArm(), Robot.getLeftHand(), Robot.getRightHand(), Robot.getLeftLeg(),
                        Robot.getRightLeg(), Robot.getLeftFoot(), Robot.getRightFoot(), Robot.getLeftShoulderMount(), Robot.getRightShoulderMount(),
                        Robot.getLeftShoulderMountJoint(), Robot.getRightShoulderMountJoint(), Robot.getLeftShoulderJoint(), Robot.getRightShoulderJoint(),
                        Robot.getLeftWristJoint(), Robot.getRightWristJoint(), Robot.getLeftHipJoint(), Robot.getRightHipJoint(), Robot.getLeftAnkleJoint(),
                        Robot.getRightAnkleJoint(), Robot.getNeckJoint()
                })
            if (part != null)
                nonNullParts.add(part);
    }

    @Test
    public void commonStructure() {
        assertThat(Robot.getContainerPart(), is(notNullValue()));
        for (Part p : nonNullParts)
            assertThat(Robot.getContainerPart().getChildren(), hasItem(p));
        for (Part p : Robot.getContainerPart().getChildren())
            assertThat(nonNullParts, hasItem(p));

        optionalJointSpecification(Robot.getTorso(), Robot.getNeckJoint(), Robot.getHead());
        optionalJointSpecification(Robot.getTorso(), Robot.getLeftShoulderMountJoint(), Robot.getLeftShoulderMount());
        optionalJointSpecification(Robot.getTorso(), Robot.getRightShoulderMountJoint(), Robot.getRightShoulderMount());
        optionalJointSpecification(Robot.getTorso(), Robot.getLeftShoulderJoint(), Robot.getLeftArm());
        optionalJointSpecification(Robot.getTorso(), Robot.getRightShoulderJoint(), Robot.getRightArm());
        optionalJointSpecification(Robot.getLeftArm(), Robot.getLeftWristJoint(), Robot.getLeftHand());
        optionalJointSpecification(Robot.getRightArm(), Robot.getRightWristJoint(), Robot.getRightHand());
        requiredJoinSpecification(Robot.getTorso(), Robot.getLeftHipJoint(), Robot.getLeftLeg());
        requiredJoinSpecification(Robot.getTorso(), Robot.getRightHipJoint(), Robot.getRightLeg());
        optionalJointSpecification(Robot.getLeftLeg(), Robot.getLeftAnkleJoint(), Robot.getLeftFoot());
        optionalJointSpecification(Robot.getRightLeg(), Robot.getRightAnkleJoint(), Robot.getRightFoot());
    }

    @Test
    public void allNonNullPartsAreDistinct() {
        Set<Part> partSet = new HashSet<>();
        for (Part p : nonNullParts)
        {
            assertThat(partSet, not(hasItem(p)));
            partSet.add(p);
        }
    }

    protected abstract RobotBuilder getRobotBuilder();

    protected void noPartsFor(Part... missing) {
        for (Part part : missing)
            assertThat(part, is(nullValue()));
    }

    private static void optionalJointSpecification(Part mainPart, Joint joint, Part extraPart) {
        if (mainPart != null && extraPart != null)
            requiredJoinSpecification(mainPart, joint, extraPart);
        else
            assertThat(joint, is(nullValue()));
    }

    private static void requiredJoinSpecification(Part end1, Joint joint, Part end2) {
        assertThat(joint, is(notNullValue()));
        assertThat(joint.getEnd1(), is(sameInstance(end1)));
        assertThat(joint.getEnd2(), is(sameInstance(end2)));
    }
}


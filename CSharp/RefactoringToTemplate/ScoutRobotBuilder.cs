﻿// Copyright 2018-2018 DevCraft, Inc.
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

using RefactoringToTemplate.Equipment;
using RefactoringToTemplate.Legs;
using RefactoringToTemplate.Torsos;

namespace RefactoringToTemplate
{
  public class ScoutRobotBuilder : RobotBuilder
  {
    private ScoutRobotBuilder()
    {
    }

    public Robot Build()
    {
      var c = new Part(null);
      var torso = new LightCockpitTorso(c);
      var laser = new Laser(c);
      var leftShoulderMountJoint = new Joint(c, torso, laser);
      var sensorPod = new SensorPod(c);
      var rightShoulderMountJoint = new Joint(c, torso, sensorPod);
      var leftLeg = new FastTread(c);
      var leftHipJoint = new Joint(c, torso, leftLeg);
      var rightLeg = new FastTread(c);
      var rightHipJoint = new Joint(c, torso, rightLeg);
      return new Robot(
        c, torso,
        null, null,
        leftShoulderMountJoint, laser, rightShoulderMountJoint, sensorPod,
        null, null, null, null,
        null, null, null, null,
        leftHipJoint, leftLeg, rightHipJoint, rightLeg,
        null, null, null, null);
    }

    public static RobotBuilder GetInstance()
    {
      return new ScoutRobotBuilder();
    }
  }
}
﻿// Copyright 2018-2018 DevCraft, Inc.
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

namespace RefactoringToTemplate
{
  public class Robot
  {
    public Robot(Part containerPart, Part torso, Joint neckJoint, Part head, Joint leftShoulderMountJoint,
      Part leftShoulderMount, Joint rightShoulderMountJoint, Part rightShoulderMount, Joint leftShoulderJoint,
      Part leftArm, Joint rightShoulderJoint, Part rightArm, Joint leftWristJoint, Part leftHand, Joint rightWristJoint,
      Part rightHand, Joint leftHipJoint, Part leftLeg, Joint rightHipJoint, Part rightLeg, Joint leftAnkleJoint,
      Part leftFoot, Joint rightAnkleJoint, Part rightFoot)
    {
      ContainerPart = containerPart;
      Torso = torso;
      NeckJoint = neckJoint;
      Head = head;
      LeftShoulderMountJoint = leftShoulderMountJoint;
      LeftShoulderMount = leftShoulderMount;
      RightShoulderMountJoint = rightShoulderMountJoint;
      RightShoulderMount = rightShoulderMount;
      LeftShoulderJoint = leftShoulderJoint;
      LeftArm = leftArm;
      RightShoulderJoint = rightShoulderJoint;
      RightArm = rightArm;
      LeftWristJoint = leftWristJoint;
      LeftHand = leftHand;
      RightWristJoint = rightWristJoint;
      RightHand = rightHand;
      LeftHipJoint = leftHipJoint;
      LeftLeg = leftLeg;
      RightHipJoint = rightHipJoint;
      RightLeg = rightLeg;
      LeftAnkleJoint = leftAnkleJoint;
      LeftFoot = leftFoot;
      RightAnkleJoint = rightAnkleJoint;
      RightFoot = rightFoot;
    }

    public Part ContainerPart { get; }
    public Part Torso { get; }
    public Joint NeckJoint { get; }
    public Part Head { get; }
    public Joint LeftShoulderMountJoint { get; }
    public Part LeftShoulderMount { get; }
    public Joint RightShoulderMountJoint { get; }
    public Part RightShoulderMount { get; }
    public Joint LeftShoulderJoint { get; }
    public Part LeftArm { get; }
    public Joint RightShoulderJoint { get; }
    public Part RightArm { get; }
    public Joint LeftWristJoint { get; }
    public Part LeftHand { get; }
    public Joint RightWristJoint { get; }
    public Part RightHand { get; }
    public Joint LeftHipJoint { get; }
    public Part LeftLeg { get; }
    public Joint RightHipJoint { get; }
    public Part RightLeg { get; }
    public Joint LeftAnkleJoint { get; }
    public Part LeftFoot { get; }
    public Joint RightAnkleJoint { get; }
    public Part RightFoot { get; }
  }
}
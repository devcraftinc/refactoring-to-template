﻿// Copyright 2018-2018 DevCraft, Inc.
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

using RefactoringToTemplate.Arms;
using RefactoringToTemplate.Feet;
using RefactoringToTemplate.Hands;
using RefactoringToTemplate.Heads;
using RefactoringToTemplate.Legs;
using RefactoringToTemplate.Torsos;

namespace RefactoringToTemplate
{
  public class StandardRobotBuilder : RobotBuilder
  {
    private StandardRobotBuilder()
    {
    }

    public Robot Build()
    {
      var c = new Part(null);
      var torso = new StockTorso(c);
      var head = new CockpitHead(c);
      var neckJoint = new Joint(c, torso, head);
      var leftArm = new StockArm(c);
      var leftShoulderJoint = new Joint(c, torso, leftArm);
      var rightArm = new StockArm(c);
      var rightShoulderJoint = new Joint(c, torso, rightArm);
      var leftHand = new ClampHand(c);
      var leftWristJoint = new Joint(c, leftArm, leftHand);
      var rightHand = new ClampHand(c);
      var rightWristJoint = new Joint(c, rightArm, rightHand);
      var leftLeg = new StockLeg(c);
      var leftHipJoint = new Joint(c, torso, leftLeg);
      var rightLeg = new StockLeg(c);
      var rightHipJoint = new Joint(c, torso, rightLeg);
      var leftFoot = new StockFoot(c);
      var leftAnkleJoint = new Joint(c, leftLeg, leftFoot);
      var rightFoot = new StockFoot(c);
      var rightAnkleJoint = new Joint(c, rightLeg, rightFoot);
      return new Robot(
        c, torso,
        neckJoint, head,
        null, null, null, null,
        leftShoulderJoint, leftArm, rightShoulderJoint, rightArm,
        leftWristJoint, leftHand, rightWristJoint, rightHand,
        leftHipJoint, leftLeg, rightHipJoint, rightLeg,
        leftAnkleJoint, leftFoot, rightAnkleJoint, rightFoot);
    }

    public static RobotBuilder GetInstance()
    {
      return new StandardRobotBuilder();
    }
  }
}
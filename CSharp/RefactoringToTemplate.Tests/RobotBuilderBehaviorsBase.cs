﻿// Copyright 2018-2018 DevCraft, Inc.
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace RefactoringToTemplate.Tests
{
  [TestClass]
  public abstract class RobotBuilderBehaviorsBase
  {
    private List<Part> nonNullParts;
    protected Robot Robot;

    [TestInitialize]
    public void Setup()
    {
      Robot = GetRobotBuilder().Build();
      nonNullParts = new List<Part>();
      foreach (var part in new[]
      {
        Robot.Head, Robot.Torso, Robot.LeftArm, Robot.RightArm, Robot.LeftHand, Robot.RightHand, Robot.LeftLeg,
        Robot.RightLeg, Robot.LeftFoot, Robot.RightFoot, Robot.LeftShoulderMount, Robot.RightShoulderMount,
        Robot.LeftShoulderMountJoint, Robot.RightShoulderMountJoint, Robot.LeftShoulderJoint, Robot.RightShoulderJoint,
        Robot.LeftWristJoint, Robot.RightWristJoint, Robot.LeftHipJoint, Robot.RightHipJoint, Robot.LeftAnkleJoint,
        Robot.RightAnkleJoint, Robot.NeckJoint
      })
        if (part != null)
          nonNullParts.Add(part);
    }

    [TestMethod]
    public void CommonStructure()
    {
      Assert.IsNotNull(Robot.ContainerPart);
      CollectionAssert.AreEquivalent(nonNullParts, new List<Part>(Robot.ContainerPart.Children));
      OptionalJointSpecification(Robot.Torso, Robot.NeckJoint, Robot.Head);
      OptionalJointSpecification(Robot.Torso, Robot.LeftShoulderMountJoint, Robot.LeftShoulderMount);
      OptionalJointSpecification(Robot.Torso, Robot.RightShoulderMountJoint, Robot.RightShoulderMount);
      OptionalJointSpecification(Robot.Torso, Robot.LeftShoulderJoint, Robot.LeftArm);
      OptionalJointSpecification(Robot.Torso, Robot.RightShoulderJoint, Robot.RightArm);
      OptionalJointSpecification(Robot.LeftArm, Robot.LeftWristJoint, Robot.LeftHand);
      OptionalJointSpecification(Robot.RightArm, Robot.RightWristJoint, Robot.RightHand);
      RequiredJoinSpecification(Robot.Torso, Robot.LeftHipJoint, Robot.LeftLeg);
      RequiredJoinSpecification(Robot.Torso, Robot.RightHipJoint, Robot.RightLeg);
      OptionalJointSpecification(Robot.LeftLeg, Robot.LeftAnkleJoint, Robot.LeftFoot);
      OptionalJointSpecification(Robot.RightLeg, Robot.RightAnkleJoint, Robot.RightFoot);
    }

    [TestMethod]
    public void AllNonNullPartsAreDistinct()
    {
      CollectionAssert.AllItemsAreUnique(nonNullParts);
    }

    protected abstract RobotBuilder GetRobotBuilder();

    protected void NoPartsFor(params Part[] missing)
    {
      foreach (var part in missing)
        Assert.IsNull(part);
    }

    private static void OptionalJointSpecification(Part mainPart, Joint joint, Part extraPart)
    {
      if (mainPart != null && extraPart != null)
        RequiredJoinSpecification(mainPart, joint, extraPart);
      else
        Assert.IsNull(joint);
    }

    private static void RequiredJoinSpecification(Part end1, Joint joint, Part end2)
    {
      Assert.IsNotNull(joint);
      Assert.AreSame(end1, joint.End1);
      Assert.AreSame(end2, joint.End2);
    }
  }
}
﻿// Copyright 2018-2018 DevCraft, Inc.
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

using Microsoft.VisualStudio.TestTools.UnitTesting;
using RefactoringToTemplate.Arms;
using RefactoringToTemplate.Feet;
using RefactoringToTemplate.Hands;
using RefactoringToTemplate.Heads;
using RefactoringToTemplate.Legs;
using RefactoringToTemplate.Torsos;

namespace RefactoringToTemplate.Tests
{
  [TestClass]
  public class StandardRobotBuilderBehaviors : RobotBuilderBehaviorsBase
  {
    [TestMethod]
    public void NoShoulderMounts()
    {
      NoPartsFor(Robot.LeftShoulderMount, Robot.RightShoulderMount);
    }

    [TestMethod]
    public void CockpitHead()
    {
      Assert.IsInstanceOfType(Robot.Head, typeof(CockpitHead));
    }

    [TestMethod]
    public void StandardTorso()
    {
      Assert.IsInstanceOfType(Robot.Torso, typeof(StockTorso));
    }

    [TestMethod]
    public void StandardLegs()
    {
      Assert.IsInstanceOfType(Robot.LeftLeg, typeof(StockLeg));
      Assert.IsInstanceOfType(Robot.RightLeg, typeof(StockLeg));
    }

    [TestMethod]
    public void StandardFeet()
    {
      Assert.IsInstanceOfType(Robot.LeftFoot, typeof(StockFoot));
      Assert.IsInstanceOfType(Robot.RightFoot, typeof(StockFoot));
    }

    [TestMethod]
    public void StandardArms()
    {
      Assert.IsInstanceOfType(Robot.LeftArm, typeof(StockArm));
      Assert.IsInstanceOfType(Robot.RightArm, typeof(StockArm));
    }

    [TestMethod]
    public void ClampHands()
    {
      Assert.IsInstanceOfType(Robot.LeftHand, typeof(ClampHand));
      Assert.IsInstanceOfType(Robot.RightHand, typeof(ClampHand));
    }

    protected override RobotBuilder GetRobotBuilder()
    {
      return StandardRobotBuilder.GetInstance();
    }
  }
}